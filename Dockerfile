FROM openjdk:11
VOLUME /tmp
# Default port but can take other value during container runtime
ENV PORT=5000
EXPOSE ${PORT}
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} kahoula-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-Dspring.profiles.active=dev", "-Dserver.port=${PORT}", "-jar","/kahoula-0.0.1-SNAPSHOT.jar"]