package com.afrologix.kahoula;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KahoulaApplication {

	public static void main(String[] args) {
		SpringApplication.run(KahoulaApplication.class, args);
	}

}
