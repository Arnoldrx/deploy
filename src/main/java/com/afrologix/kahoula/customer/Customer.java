package com.afrologix.kahoula.customer;

import com.afrologix.kahoula.user.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "customer")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 20)
    @Column(name = "firstname", nullable = false)
    private String firstname;

    @NotNull
    @Size(max = 20)
    @Column(name = "lastname", nullable = false)
    private String lastname;

    @NotNull
    @Size(max = 20)
    @Column(name = "testimony", nullable = false)
    private String testimony;

    @OneToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

}
