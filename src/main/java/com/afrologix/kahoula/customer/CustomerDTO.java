package com.afrologix.kahoula.customer;

import com.afrologix.kahoula.user.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDTO {

    private Long id;

    private String firstname;

    private String lastname;

    private String testimony;

    private User user;
}
