package com.afrologix.kahoula.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    //add a record
    public Customer saveCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    //Get all the professionals
    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    //get a specific record by Id
    public Customer getCustomerById(Long id) {
        return customerRepository.findById(id).orElse(null);
    }

    //Update a customer
    public Customer updateCustomer(Long id, Customer customer) {
        customerRepository.findById(id);
        customer.setId(id);
        return customerRepository.save(customer);
    }

    //delete a specific record
    public void deleteById(Long id) {
        customerRepository.deleteById(id);
    }
}
