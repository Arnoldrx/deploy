package com.afrologix.kahoula.professional;


import com.afrologix.kahoula.user.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "professional")
public class Professional {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @NotNull
    @Size(max = 20)
    @Column(name = "firstname", nullable = false)
    private String firstname;


    @NotNull
    @Size(max = 20)
    @Column(name = "lastname", nullable = false)
    private String lastname;


    @NotNull
    @Size(max = 20)
    @Column(name = "location", nullable = false)
    private String location;


    @NotNull
    @Column(name = "rating", nullable = false)
    private Long rating;


    @NotNull
    @Size(max = 20)
    @Column(name = "jobtype", nullable = false)
    private String jobType;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
}
