package com.afrologix.kahoula.professional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api")
public class ProfessionalController {

    private ProfessionalService professionalService;

    public ProfessionalController(ProfessionalService professionalService) {
        this.professionalService = professionalService;
    }

    @GetMapping("/professional")
    public ResponseEntity<List<Professional>> getAllProfessionals() {
        List<Professional> list = professionalService.getAllProfessionals();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/professional/{id}")
    public ResponseEntity<Professional> getProfessionalById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(professionalService.getProfessionalById(id), HttpStatus.OK);
    }

    @PostMapping("/professional")
    public ResponseEntity<Professional> saveProfessional(@RequestBody Professional professional) {
        Professional professional1 = professionalService.saveProfessional(professional);
        return new ResponseEntity<>(professional1, HttpStatus.CREATED);
    }


    @DeleteMapping("/professional/{id}")
    public ResponseEntity<HttpStatus> deleteProfessionalById(@PathVariable("id") Long id) {
        professionalService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);

    }

    @PutMapping("/professional/update/{id}")
    public ResponseEntity<Professional> updateProfessional(@RequestBody Professional professional, @PathVariable Long id) {
        return new ResponseEntity<>(professionalService.updateProfessional(id, professional), HttpStatus.OK);
    }

}