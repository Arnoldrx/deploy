package com.afrologix.kahoula.professional;

import com.afrologix.kahoula.user.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProfessionalDTO {
    private Long id;

    private String firstname;

    private String lastname;

    private String location;

    private Long rating;

    private String jobType;

    private User user;
}
