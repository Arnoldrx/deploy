package com.afrologix.kahoula.professional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProfessionalService {

    @Autowired
    private ProfessionalRepository professionalRepository;

    public ProfessionalService(ProfessionalRepository professionalRepository) {
        this.professionalRepository = professionalRepository;
    }

    //add a record
    public Professional saveProfessional(Professional professional) {
        return professionalRepository.save(professional);
    }

    //Get all the professionals
    public List<Professional> getAllProfessionals() {
        return professionalRepository.findAll();
    }

    //Update a professional
    public Professional updateProfessional(Long id, Professional professional) {
        professionalRepository.findById(id);
        professional.setId(id);
        return professionalRepository.save(professional);
    }

    //get a specific record by Id
    public Professional getProfessionalById(Long id) {
        return professionalRepository.findById(id).orElse(null);
    }

    //delete a specific record
    public void deleteById(Long id) {
        professionalRepository.deleteById(id);
    }

}
