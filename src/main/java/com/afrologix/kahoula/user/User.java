package com.afrologix.kahoula.user;


import com.afrologix.kahoula.authority.Authority;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;


@Data
@NoArgsConstructor
@Entity
@Table(name = "user",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "username"),
                @UniqueConstraint(columnNames = "email")
        }
)
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull@Size(max = 20)
    @NotNull
    @Size(max = 20)
    @Column(name = "username", nullable = false)
    private String username;

    @NotNull@Size(max = 20)@Email
    @NotNull
    @Size(max = 20)
    @Email
    private String email;

    @NotNull@Size(max = 120)
    @NotNull
    @Size(max = 120)
    @Column(name = "password_hash", nullable = false)
    private String passwordHash;


    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "user_authority",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "authority_name", referencedColumnName = "name"))
    private Set<Authority> authorities = new HashSet<>();

    public User(Long id, @NotNull @Size(max = 20) String username, @NotNull @Size(max = 20) @Email String email, @NotNull @Size(max = 120) String passwordHash) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.passwordHash = passwordHash;
    }

}
