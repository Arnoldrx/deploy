package com.afrologix.kahoula.authority;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@DataJpaTest
public class AuthorityRepositoryTest {

    @Autowired
    private AuthorityRepository authorityRepository;
    private Authority authority;

    @Before
    public void setUp() {
        authority = new Authority("ROLE_ADMIN");
    }

    //Save a professional
    @Test
    public void testSaveAuthority() {
        authorityRepository.save(authority);
        Optional<Authority> authority1 = authorityRepository.findByName("ROLE_ADMIN");
        assertNotNull(authority);
        assertEquals(authority1.get().getName(), authority.getName());
    }

    //Get all Authorities
    @Test
    public void GivenGetAllAuthoritiesShouldReturnListOfAllAuthorities(){
        Authority authority1 = new Authority("ROLE_CLIENT");
        authorityRepository.save(authority);
        authorityRepository.save(authority1);
        List<Authority> authorityList = (List<Authority>) authorityRepository.findAll();
        assertEquals("ROLE_CLIENT", authorityList.get(1).getName());
    }

    //Retrieve Authority by Name
    @Test
    public void givenAuthorityNameThenShouldReturnTheAuthority() {
        Authority authority1 = authorityRepository.save(authority);
        Optional<Authority> optional = authorityRepository.findByName(authority1.getName());
        assertEquals(authority1.getName(), optional.get().getName());
        assertEquals(authority1.getName(), optional.get().getName());
    }

    //Delete Authority by Id
    @Test
    public void givenIdTODeleteThenShouldDeleteTheAuthority() {
        authorityRepository.save(authority);
        authorityRepository.deleteById(authority.getName());
        Optional optional = authorityRepository.findById(authority.getName());
        assertEquals(Optional.empty(), optional);;
    }
}