package com.afrologix.kahoula.customer;

import com.afrologix.kahoula.user.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class CustomerControllerTest {

    @Mock
    private Customer customer;

    @Mock
    private CustomerService customerService;

    @Mock
    private List<Customer> customerList;

    @InjectMocks
    private CustomerController customerController;

    @Autowired
    MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        customer = new Customer(
                1L,
                "afro",
                "logix",
                "Software Dev",
                new User(2L, "afrologix", "contact@gmail.com", "sdunkdsn23894fkl2$&*")
        );
        mockMvc = MockMvcBuilders.standaloneSetup(customerController).build();
    }

    @AfterEach
    void tearDown() {
        customer = null;
    }

    @Test
    void getAllCustomers() throws Exception {
        when(customerService.getAllCustomers()).thenReturn(customerList);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/api/customer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(customer)))
                .andDo(MockMvcResultHandlers.print());

        verify(customerService).getAllCustomers();
        verify(customerService, times(1)).getAllCustomers();
    }

    @Test
    void getCustomerById() throws Exception {
        when(customerService.getCustomerById(customer.getId())).thenReturn(customer);
        mockMvc.perform(MockMvcRequestBuilders.
                get("/api/customer/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(customer)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void saveCustomer() throws Exception {
        when(customerService.saveCustomer(any())).thenReturn(customer);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/customer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(customer)))
                .andExpect(status().isCreated());
        verify(customerService, times(1)).saveCustomer(any());
    }

    @Test
    void deleteCustomerById() throws Exception {
        doNothing().when(customerService).deleteById(customer.getId());
        mockMvc.perform(
                delete("/api/customer/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(customer)))
                .andExpect(status().isNoContent())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void updateCustomer() throws Exception {
        mockMvc.perform(
                put("/api/customer/update/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(customer)))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}