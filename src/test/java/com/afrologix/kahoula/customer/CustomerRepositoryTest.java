package com.afrologix.kahoula.customer;

import com.afrologix.kahoula.user.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CustomerRepositoryTest {

    @Autowired
    private CustomerRepository customerRepository;
    private Customer customer;

    @Before
    public void setUp()  {
        customer = new Customer(
                1L,
                "ok",
                "john",
                "It's a nice platform",
                new User(1L, "afrologix", "contact@gmail.com", "sdunkdsn23894fkl2$&*"));
    }



    //Save a Customer
    @Test
    public void givenCustomerToAddShouldAddTheCustomer() {
        customerRepository.save(customer);
        Customer customer1 = customerRepository.findById(customer.getId()).get();
        assertNotNull(customer1);
        assertEquals("ok", customer1.getFirstname());
    }

    //Get all professionals
    @Test
    public void givenAllCustomerShouldReturnListOfAllCustomer() {
        customerRepository.save(customer);
        List<Customer> customerList = (List<Customer>)customerRepository.findAll();
        assertEquals("ok", customerList.get(0).getFirstname());
    }


    //Retrieve customer by Id
    @Test
    public void givenIdThenShouldReturnCustomerOfThatId() {
        Customer customer1 = customerRepository.save(customer);
        Optional<Customer> optional = customerRepository.findById(customer1.getId());
        assertEquals(customer1.getId(), optional.get().getId());
        assertEquals(customer1.getFirstname(), optional.get().getFirstname());
    }

    //Delete customer by Id
    @Test
    public void givenIdTODeleteThenShouldDeleteTheCustomer() {
        customerRepository.save(customer);
        customerRepository.deleteById(customer.getId());
        Optional optional = customerRepository.findById(customer.getId());
        assertEquals(Optional.empty(), optional);;
    }
}