package com.afrologix.kahoula.customer;

import com.afrologix.kahoula.user.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CustomerServiceTest {

    @Mock
    CustomerRepository customerRepository;

    @Mock
    private Customer customer;

    @Autowired
    @InjectMocks
    private CustomerService customerService;
    List<Customer> customerList;

    @BeforeEach
    void setUp() {
        customerList = new ArrayList<>();
        customer = new Customer(
                1L,
                "afro",
                "logix",
                "Actually Good",
                new User(1L, "afrologix", "contact@gmail.com", "sdunkdsn23894fkl2$&*")
        );
        customerList.add(customer);
    }

    @AfterEach
    void tearDown() {
        customer = null;
        customerList = null;
    }

    @Test
    void saveCustomer() {
        when(customerRepository.save(any())).thenReturn(customer);
        customerService.saveCustomer(customer);
        verify(customerRepository, times(1)).save(any());
    }

    @Test
    void getAllCustomers() {
        customerRepository.save(customer);

        when(customerRepository.findAll()).thenReturn(customerList);
        List<Customer> customerList1 = customerService.getAllCustomers();
        assertEquals(customerList1, customerList);

        verify(customerRepository, times(1)).save(customer);
        verify(customerRepository, times(1)).findAll();
    }

    @Test
    void getCustomerById() {
        when(customerRepository.findById(customer.getId())).thenReturn(Optional.ofNullable(customer));
        assertThat(customerService.getCustomerById(customer.getId())).isEqualTo(customer);
    }

    @Test
    void deleteById() {
        customerService.deleteById(customer.getId());
        verify(customerRepository, times(1)).deleteById(customer.getId());
    }

    @Test
    void updateCustomer() {
        Customer customer = new Customer();
        customer.setFirstname("rodrigue");

        customerService.updateCustomer(customer.getId(), customer);
        verify(customerRepository).save(customer);
        verify(customerRepository).findById(customer.getId());
    }
}