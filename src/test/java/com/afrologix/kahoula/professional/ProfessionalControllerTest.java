package com.afrologix.kahoula.professional;

import com.afrologix.kahoula.user.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class ProfessionalControllerTest {

    @Mock
    private Professional professional;

    @Mock
    private ProfessionalService professionalService;

    @Mock
    private List<Professional> professionalList;

    @InjectMocks
    private ProfessionalController professionalController;

    @Autowired
    MockMvc mockMvc;


    @BeforeEach
    public void setUp() {
        professional = new Professional(
                1L,
                "afro",
                "logix",
                "Yaounde",
                5L,
                "Software Dev",
                new User(2L, "afrologix", "contact@gmail.com", "sdunkdsn23894fkl2$&*")
        );
        mockMvc = MockMvcBuilders.standaloneSetup(professionalController).build();
    }

    @Test
    void getAllProfessionals() throws Exception {
        when(professionalService.getAllProfessionals()).thenReturn(professionalList);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/api/professional")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(professional)))
                .andDo(MockMvcResultHandlers.print());

        verify(professionalService).getAllProfessionals();
        verify(professionalService, times(1)).getAllProfessionals();
    }

    @Test
    void getProfessionalById() throws Exception {
        when(professionalService.getProfessionalById(professional.getId())).thenReturn(professional);
        mockMvc.perform(MockMvcRequestBuilders.
                get("/api/professional/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(professional)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void saveProfessional() throws Exception{
        when(professionalService.saveProfessional(any())).thenReturn(professional);
        mockMvc.perform(MockMvcRequestBuilders
                .post("/api/professional")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(professional)))
                .andExpect(status().isCreated());
        verify(professionalService, times(1)).saveProfessional(any());
    }

    @Test
    void deleteProfessionalById() throws Exception {
        doNothing().when(professionalService).deleteById(professional.getId());
        mockMvc.perform(
                delete("/api/professional/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(professional)))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    void updateProfessional() throws Exception {
        mockMvc.perform(
                put("/api/professional/update/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(professional)))
                .andExpect(status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
