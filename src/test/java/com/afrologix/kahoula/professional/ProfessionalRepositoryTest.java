package com.afrologix.kahoula.professional;

import com.afrologix.kahoula.user.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProfessionalRepositoryTest {

    @Autowired
    private ProfessionalRepository professionalRepository;
    private Professional professional;

    @Before
    public void setUp() {
        professional = new Professional(
                1L,
                "afro",
                "logix",
                "yaounde",
                4L,
                "software",
                new User(1L, "afrologix", "contact@gmail.com", "sdunkdsn23894fkl2$&*"));

    }

    //Save a Professional
    @Test
    public void givenProfessionalToAddShouldAddProfessional() {
        professionalRepository.save(professional);
        Professional professional1 = professionalRepository.findById(professional.getId()).get();
        assertNotNull(professional1);
        assertEquals("yaounde", professional1.getLocation());
    }

    //Get all professionals
    @Test
    public void givenAllProfessionalShouldReturnListOfAllProfessional() {
        professionalRepository.save(professional);
        List<Professional> professionalList = (List<Professional>)professionalRepository.findAll();
        assertEquals("afro", professionalList.get(0).getFirstname());
    }


    //Retrieve a Professional by Id
    @Test
    public void givenIdThenShouldReturnProfessionalOfThatId() {
        Professional professional1 = professionalRepository.save(professional);
        Optional<Professional> optional = professionalRepository.findById(professional1.getId());
        assertEquals(professional1.getId(), optional.get().getId());
        assertEquals(professional1.getFirstname(), optional.get().getFirstname());
    }

    //Delete a Professional by id
    @Test
    public void givenIdTODeleteThenShouldDeleteTheProfessional() {
        professionalRepository.save(professional);
        professionalRepository.deleteById(professional.getId());
        Optional optional = professionalRepository.findById(professional.getId());
        assertEquals(Optional.empty(), optional);;
    }
}