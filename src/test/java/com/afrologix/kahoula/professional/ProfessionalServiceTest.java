package com.afrologix.kahoula.professional;

import com.afrologix.kahoula.user.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProfessionalServiceTest {

    @Mock
    ProfessionalRepository professionalRepository;

    @Mock
    private Professional professional;

    @Autowired
    @InjectMocks
    private ProfessionalService professionalService;
    List<Professional> professionalList;


    @BeforeEach
    void setUp() {
        professionalList = new ArrayList<>();
        professional = new Professional(
                1L,
                "afro",
                "logix",
                "Yaounde",
                5L,
                "Software Dev",
                new User(1L, "afrologix", "contact@gmail.com", "sdunkdsn23894fkl2$&*")
        );
        professionalList.add(professional);
    }

    @AfterEach
    void tearDown() {
        professional = null;
        professionalList = null;
    }

    @Test
    void saveProfessional() {
        when(professionalRepository.save(any())).thenReturn(professional);
        professionalService.saveProfessional(professional);
        verify(professionalRepository, times(1)).save(any());
    }

    @Test
    void getAllProfessionals() {
        professionalRepository.save(professional);

        when(professionalRepository.findAll()).thenReturn(professionalList);
        List<Professional> professionalList1 = professionalService.getAllProfessionals();
        assertEquals(professionalList1, professionalList);

        verify(professionalRepository, times(1)).save(professional);
        verify(professionalRepository, times(1)).findAll();
    }

    @Test
    void getProfessionalById() {
        when(professionalRepository.findById(professional.getId())).thenReturn(Optional.ofNullable(professional));
        assertThat(professionalService.getProfessionalById(professional.getId())).isEqualTo(professional);
    }

    @Test
    void deleteById() {
        professionalService.deleteById(professional.getId());
        verify(professionalRepository, times(1)).deleteById(professional.getId());
    }

    @Test
    void updateProfessional() {
        Professional professional = new Professional();
        professional.setFirstname("rodrue");

        professionalService.updateProfessional(professional.getId(), professional);
        verify(professionalRepository).save(professional);
        verify(professionalRepository).findById(professional.getId());
    }
}