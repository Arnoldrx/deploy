package com.afrologix.kahoula.user;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    private User user;

    @Before
    public void setUp() {
        user = new User(1L, "afrologix", "contact@gmail.com", "sdunkdsn23894fkl2$&*");
    }

    //Save
    @Test
    public void givenUserToAddShouldReturnAddedUser() {
        userRepository.save(user);
        Optional<User> user1 = userRepository.findOneByUsername("afrologix");
        assertNotNull(user1);
        assertEquals(user1.get().getUsername(), user.getUsername());
    }

    //Get all Users
    @Test
    public void GivenGetAllUserShouldReturnListOfAllUsers(){
        User user1 = new User(2L, "afro", "cont@gmail.com", "sdkdsn23894fkl2$&*");
        userRepository.save(user);
        userRepository.save(user1);
        List<User> userList = (List<User>) userRepository.findAll();
        assertEquals("afro", userList.get(1).getUsername());
    }

    //Retrive User by Id
    @Test
    public void givenIdThenShouldReturnUserOfThatId() {
        User user1 = userRepository.save(user);
        Optional<User> optional = userRepository.findById(user1.getId());
        assertEquals(user1.getId(), optional.get().getId());
        assertEquals(user1.getUsername(), optional.get().getUsername());
    }

    //Delete Product By id
    @Test
    public void givenIdTODeleteThenShouldDeleteTheUser() {
        userRepository.save(user);
        userRepository.deleteById(user.getId());
        Optional optional = userRepository.findById(user.getId());
        assertEquals(Optional.empty(), optional);;
    }
}